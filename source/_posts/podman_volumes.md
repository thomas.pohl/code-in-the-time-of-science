# Root-less Podman and Mounting Host Volumes

mariadb cannot access host folders which are mounter as volumes:
```
podman run -p 3306:3306 -e MARIADB_ROOT_PASSWORD=xxx -v ./db:/var/lib/mysql -v ./conf.d:/etc/mysql/conf.d --rm --name database mariadb:10

(Errcode: 13 "Permission denied")
Fatal error in defaults handling. Program aborted
```

Find out correct user and group running mariadb:
```
podman run -ti --rm mariadb:10 cat /etc/passwd
```

Or run mariadb with SELinux disabled and do a `ps auxn`:
```
podman run -p 3306:3306 -e MARIADB_ROOT_PASSWORD=xxx -v ./db:/var/lib/mysql -v ./conf.d:/etc/mysql/conf.d --rm --security-opt label=disable --name database mariadb:10
```


Allow access to directories:
```
podman unshare chown 999:999 -R db
podman unshare chown 999:999 -R conf.d
```

Add `:Z` to volumes:
```
podman run -p 3306:3306 -e MARIADB_ROOT_PASSWORD=xxx -v ./db:/var/lib/mysql:Z -v ./conf.d:/etc/mysql/conf.d:Z --rm --name database mariadb:10
```

Done
